package Task_1;

public class CatImpl implements IAnimal {
    private String animal;
    public CatImpl(String animal) {
        this.animal = animal;
    }
    @Override
    public String getSpecies() {
        return animal;
    }
    @Override
    public void getSound() {
        System.out.println("Meow");
    }
        }

