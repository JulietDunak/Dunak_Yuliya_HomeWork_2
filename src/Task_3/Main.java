package Task_3;

public class Main {
    public static void main(String[] args) {
    Circle circle = new Circle(2.4);
        System.out.println("The area of the circle is " + circle.getArea());
        System.out.println("The perimeter of the circle is " + circle.getPerimeter());
        Square square = new Square(2);
        System.out.println("The area of the square is " + square.getArea());
        System.out.println("The perimeter of the square is " + square.getPerimeter());
}
    }
