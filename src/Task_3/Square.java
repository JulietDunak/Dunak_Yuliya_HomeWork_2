package Task_3;

public class Square extends GeometricFigure {
    private double side;
    public Square(double side) {
        this.side = side;
    }
    @Override
    public double getArea() {
                return side*side;
    }
    @Override
    public double getPerimeter() {
        return 4*side;
    }
}
