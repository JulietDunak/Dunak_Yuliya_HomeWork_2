package Task_3;
public abstract class GeometricFigure {
public abstract double getArea();
public abstract double getPerimeter();
}
